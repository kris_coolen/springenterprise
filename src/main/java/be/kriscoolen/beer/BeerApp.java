package be.kriscoolen.beer;

import be.kriscoolen.beer.domain.Beer;
import be.kriscoolen.beer.domain.BeerOrder;
import be.kriscoolen.beer.domain.BeerOrderItem;
import be.kriscoolen.beer.repository.BeerDao;
import be.kriscoolen.beer.services.BeerOrderRepository;
import be.kriscoolen.beer.services.BeerRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class BeerApp {
    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(BeerApp.class,args);
        //opdracht2
        System.out.println("*************");
        System.out.println("**opdracht2**");
        System.out.println("*************");
        BeerDao dao = ctx.getBean("beerDao",BeerDao.class);
        System.out.println(dao.getBeerById(20));
        dao.setStock(10,200);
        System.out.println(dao.getBeerByAlcohol(1.0f));
        System.out.println("*************");
        System.out.println("**opdracht3**");
        System.out.println("*************");
        BeerRepository br = ctx.getBean("beerRepository",BeerRepository.class);
        System.out.println("Beer with id=20:");
        Beer beer = br.getBeerById(20);
        System.out.println(beer);
        System.out.println("Beers with alcohol 5.0:");
        List<Beer> beers = br.getBeerByAlcohol(5.0f);
        System.out.println(beers);
        Beer updateBeer = br.getBeerById(4);
        System.out.println("Beer with id 4:");
        System.out.println(updateBeer);
        System.out.println("We update the stock to 300 and price to 3.50");
        updateBeer.setStock(300);
        updateBeer.setPrice(3.5f);
        br.updateBeer(updateBeer);
        System.out.println("Check in DB if beer is updated:");
        System.out.println(br.getBeerById(4));
        System.out.println("Now we will create a beerOrder existing of twee BeerOrderItems:");
        Beer beer1 = br.getBeerById(40);
        BeerOrderItem item1 = new BeerOrderItem();
        item1.setBeer(beer1);
        item1.setNumber(24);
        Beer beer2 = br.getBeerById(103);
        BeerOrderItem item2 = new BeerOrderItem();
        item2.setBeer(beer2);
        item2.setNumber(6);
        BeerOrder beerOrder = new BeerOrder();
        List<BeerOrderItem> beerOrderItemList = new ArrayList<>();
        beerOrderItemList.add(item1);
        beerOrderItemList.add(item2);
        beerOrder.setName("Bestelling Kris");
        beerOrder.setItems(beerOrderItemList);
        BeerOrderRepository beerOrderRepository = ctx.getBean("beerOrderRepository",BeerOrderRepository.class);
        beerOrderRepository.saveOrder(beerOrder);

    }
}
