package be.kriscoolen.beer.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository("beerDao")
public class BeerDaoImpl implements BeerDao {
    private final static String QUERY_ID = "SELECT Name, Alcohol, Price, Stock FROM Beers WHERE Id=?";
    private final static String UPDATE_STOCK = "UPDATE Beers SET Stock=? WHERE Id=?";
    private final static String QUERY_ALCOHOL=
            "SELECT Name FROM Beers WHERE Alcohol=?";
    private JdbcTemplate template;

    @Autowired
    public void setJdbcTemplate(JdbcTemplate template){
        this.template=template;
    }

    @Override
    public String getBeerById(int id) {
        Map<String,Object> result = template.queryForMap(QUERY_ID,id);
        return String.format("%s %s %s %s",
                result.get("name"),
                result.get("alcohol"),
                result.get("price"),
                result.get("stock"));
    }

    @Override
    public void setStock(int id, int stock) {
        template.update(UPDATE_STOCK,stock,id);
    }

    @Override
    public List<String> getBeerByAlcohol(float alcohol) {
        List<Map<String, Object>> resultListMap = template.queryForList(QUERY_ALCOHOL,alcohol);
        List<String> resultList = new ArrayList<>();
        for(Map<String, Object> result:resultListMap){
            resultList.add(String.format("%s",result.get("name")));
        }
        return resultList;
    }
}
