package be.kriscoolen.beer.services;

import be.kriscoolen.beer.domain.Beer;
import be.kriscoolen.beer.domain.BeerOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

@Repository("beerOrderRepository")
public class BeerOrderRepositoryImpl implements BeerOrderRepository {
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public int saveOrder(BeerOrder order) {
        EntityManager em = null;
        EntityTransaction tx = null;
        int id = 0;
        try{
            em = entityManagerFactory.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            em.persist(order);
            tx.commit();
            id = order.getId();
        }catch(Exception e){
            tx.rollback();
        }
        finally {
            if(em!=null) em.close();
        }
        return id;
    }

    @Override
    public BeerOrder getBeerOrderById(int id) {
        EntityManager em = null;
        EntityTransaction tx = null;
        BeerOrder beerOrder = null;
        try{
            em = entityManagerFactory.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            beerOrder  = em.find(BeerOrder.class,id);
            tx.commit();
        }catch(Exception e){
            tx.rollback();
        }
        finally {
            if(em!=null) em.close();
        }
        return beerOrder;
    }
}
