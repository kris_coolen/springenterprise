package be.kriscoolen.beer.services;

import be.kriscoolen.beer.domain.BeerOrder;

public interface BeerOrderRepository {
    public int saveOrder(BeerOrder order);
    public BeerOrder getBeerOrderById(int id);
}
