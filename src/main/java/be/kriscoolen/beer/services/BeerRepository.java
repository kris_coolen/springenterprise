package be.kriscoolen.beer.services;

import be.kriscoolen.beer.domain.Beer;

import java.util.List;

public interface BeerRepository {
    public Beer getBeerById(int id);
    public List<Beer> getBeerByAlcohol(float alcohol);
    public void updateBeer(Beer beer);
}
