package be.kriscoolen.beer.services;

import be.kriscoolen.beer.domain.Beer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.List;

@Repository("beerRepository")
public class BeerRepositoryImpl implements BeerRepository {
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public Beer getBeerById(int id) {
        EntityManager em = null;
        EntityTransaction tx = null;
        Beer beer = null;
        try{
            em = entityManagerFactory.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            beer  = em.find(Beer.class,id);
            tx.commit();
        }catch(Exception e){
            tx.rollback();
        }
        finally {
            if(em!=null) em.close();
        }
        return beer;
    }

    @Override
    public List<Beer> getBeerByAlcohol(float alcohol) {

        List<Beer> beers = null;
        EntityManager em = null;
        try{
            em = entityManagerFactory.createEntityManager();
            TypedQuery<Beer> query = em.createQuery("select b from Beer as b where b.alcohol=:PARAM_ALCOHOL",Beer.class);
            query.setParameter("PARAM_ALCOHOL",alcohol);
            beers = query.getResultList();
        }
        catch(Exception e){
        }
        finally{
            if(em!=null) em.close();
        }
        return beers;

    }

    @Override
    public void updateBeer(Beer beer) {
        EntityManager em = null;
        EntityTransaction tx = null;
        try{
            em = entityManagerFactory.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            Beer beerDB = em.find(Beer.class,beer.getId());
            tx.commit();
            if(beerDB!=null){
                tx.begin();
                beerDB = beer;
                em.merge(beerDB);
                tx.commit();
            }
        }catch(Exception e){
            tx.rollback();
        }
        finally {
            if(em!=null) em.close();
        }
    }
}
