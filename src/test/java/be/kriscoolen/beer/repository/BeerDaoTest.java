package be.kriscoolen.beer.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class BeerDaoTest {

    @Autowired
    private BeerDao dao;

    @Test
    public void testGetBeerById(){
        String beer = dao.getBeerById(1);
        Assertions.assertEquals("TestBeer 7.0 2.75 100",beer);
    }
    @Test
    public void testSetStock(){
        String beer = dao.getBeerById(2);
        String[] beerArray = beer.split(" ");
        String stockArray = beerArray[3];
        Assertions.assertEquals("100",stockArray);
        dao.setStock(2,200);
        String beerChanged = dao.getBeerById(2);
        String[] beerArray2 = beerChanged.split(" ");
        String stockArray2 = beerArray2[3];
        Assertions.assertEquals("200",stockArray2);

    }

    @Test
    public void testGetBeerByAlcohol(){

        Assertions.assertTrue(dao.getBeerByAlcohol(6.5f).size()==2);
    }
}
