package be.kriscoolen.beer.repository;

import be.kriscoolen.beer.domain.Beer;
import be.kriscoolen.beer.services.BeerRepository;
//import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

//@ExtendWith(SpringExtension.class)
@SpringBootTest
public class BeerRepositoryTest {

    @Autowired
    private BeerRepository beerRepository;

    //INSERT INTO Beers VALUES (1,'TestBeer',1,1,2.75,100,7,0,NULL);

    @Test
    @DirtiesContext
    public void getBeerByIdTest(){
        Beer beer = beerRepository.getBeerById(1);
        assertEquals(1,beer.getId());
        assertEquals("TestBeer",beer.getName());
        assertEquals(1,beer.getBrewer().getId());
        assertEquals(1,beer.getCategory().getId());
        assertEquals(2.75f,beer.getPrice());
        assertEquals(100,beer.getStock());
        assertEquals(7.0,beer.getAlcohol());
        assertEquals(0,beer.getVersion());
    }

    @Test
    @DirtiesContext
    public void getBeerByAlcoholTest(){
        assertEquals(2,beerRepository.getBeerByAlcohol(6.5f).size());
        assertEquals(0,beerRepository.getBeerByAlcohol(5.0f).size());
    }

    @Test
    @DirtiesContext
    public void updateBeerTest(){
        Beer beer = beerRepository.getBeerById(1);
        System.out.println(beer);
        beer.setPrice(4.5f);
        beerRepository.updateBeer(beer);
        assertEquals(4.5f,beerRepository.getBeerById(1).getPrice());
        //nu een een nieuw beer object maken. Wel zeker id geven die bestaat!
        Beer beer2 = new Beer();
        beer2.setId(2);
        //hiemee willen we dus het bestaande bier in de DB met id 2 gaan updaten.
        //we passen bijvoorbeeld de prijs en stock aan.
        beer2.setPrice(5.45f);
        beer2.setStock(1000);
        beerRepository.updateBeer(beer2);
        //de overige velden zijn nu normaal niet ingevuld! dit is dus geen goede manier om een update te doen!!!!
        //maar het is wel mogelijk...
        Beer beerDB2 = beerRepository.getBeerById(2);
        assertEquals(5.45f,beerDB2.getPrice());
        assertEquals(1000,beerDB2.getStock());
        assertEquals(null,beerDB2.getName());
    }
}
